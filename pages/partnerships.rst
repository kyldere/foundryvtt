.. _partnerships:

Community Partnerships
**********************

Foundry Virtual Tabletop is proud to have established partnerships with several other teams or creators who are
producing fantastic software and content in the virtual tabletop space.

Forgotten Adventures
====================

.. image:: /_static/images/forgotten-adventures.png
    :align: left
    :target: https://www.patreon.com/forgottenadventures/

Forgotten Adventures is an artistic community of virtual tabletop gaming enthusiasts organized by Stryxin, a devoted
artist and dungeon master who creates tokens, props, textures, and battlemaps specifically designed with virtual 
tabletop gaming in mind. Patreon supporters of Forgotten Adventures gain access to the complete portfolio of assets
with new releases arriving several times per month. Stryxin is even tackling an incredibly ambitious goal to draw 
creature tokens for every D&D5e monster.

I am thrilled to partner with Stryxin and Forgotten Adventures to showcase his fantastic artwork and make it available
to Foundry Virtual Tabletop users as part of our D&D5e system implementation which features token artwork by Stryxin
for all the creatures which have currently been completed - eventually covering the entire Monsters SRD compendium.

These included tokens are just a subset of the amazing work available in this community so please be sure to support
Forgotten Adventures on Patreon for even more content. Visit https://www.patreon.com/forgottenadventures/ for more
information!


Dungeon Fog
===========

.. image:: /_static/images/dungeon-fog.png
    :align: left
    :target: https://www.dungeonfog.com/

Dungeon Fog is an online map maker & authoring tool for RPG game masters. With the extensive DUNGEONFOG Editor you
can draw your RPG tabletop maps with just a few clicks – no more patching up map tiles! Create multi-level dungeons,
terrain, or entire worlds in an instant. Generate your GM-Notes automatically and export or print high-res images and
notes, or send a Fog of War version for your players to your TV!

Thanks to the hard work of community modders, Foundry Virtual Tabletop features a Dungeon Fog integration module which
can automatically configure walls and doors for the fog of war and ambient lighting system automatically by exporting
data directly from Dungeon Fog and importing into Foundry VTT. Learn more on the :ref:`modules` page.

To celebrate our partnership, you can use the discount code **FOUNDRY** when subscribing to the Dungeon Fog
service for a 10% discount on the price of subscription! Visit https://www.dungeonfog.com/foundry to take
advantage of this offer!


DunGen.app
==========

.. image:: /_static/images/dungen.png
    :align: left
    :target: https://dungen.app/

DunGen is a Dungeon Generator that creates high resolution maps ready to import into Foundry VTT. It offers many room
variations with several themes to choose from, and uses a custom algorithm to try and generate a logical dungeon.
DunGen is still in development and updated often with new rooms, themes and features.

In addition to the generated map images themselves, you can also
generate a full scene file including pre-built walls to take full advantage of Foundry's dynamic lighting. You can see this integration in action in `this short video <https://youtu.be/2RlPpLOFkhc>`_.